import React, {useState} from "react";
import {Layout, Typography, Menu} from "antd";
import LayoutSide from "./LayoutSide";
import LayoutHeader from "./LayoutHeader";
import {HomeOutlined} from "@ant-design/icons";

const {Sider, Header, Content, Footer} = Layout;
const {Paragraph, Text} = Typography;


const LayoutMain = ({children}) => {
    const [collapsed, setCollapsed] = useState(false);
    return <div>
        <Layout

            hasSider={true}>
            <Sider collapsible
                   collapsed={collapsed}
                   style={{
                       height: "100vh",
                       color : 'white'
                   }}
                   onCollapse={value => setCollapsed(value)}
            >
                <LayoutSide/>
            </Sider>
            <Layout>
                <LayoutHeader/>
                <Content className={"pr-4 pl-5"}>
                    {children}
                </Content>
            </Layout>
        </Layout>
    </div>
}

export default LayoutMain;

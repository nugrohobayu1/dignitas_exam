import React from 'react';
import {Menu, Typography} from "antd";
import {BookOutlined, HomeOutlined, UserOutlined} from "@ant-design/icons";
import Link from 'next/link';
import {useRouter} from "next/router";

const menuList = [
        {
                name : "Dashboard" ,
                key:"dashboard",
                icon : <HomeOutlined style={{fontsize:'26px', color:'#ffffff'}}/>,
                url: '/dashboard'
        },
        {
                name : "Pemesanan",
                key:"pemesanan",
                icon : <BookOutlined style={{fontsize:'26px', color:'#ffffff'}}/>,
                url: '/pemesanan'
        },
        {
                name : "Customer",
                key:"customer",
                icon : <UserOutlined style={{fontsize:'26px', color:'#ffffff'}}/>,
                url: '/customer'
        }
        
]

const LayoutSide = (props) => {
        const router = useRouter();

        return <Menu theme="dark" className={'text-lg menu-color'} defaultSelectedKeys={['1']} mode="inline"
                     style={{marginTop: '10vh', color: '#ffffff'}}>
                        {menuList.map(menu => {
                                return <Menu.Item
                                    className={'text-lg text-white'}
                                    key={menu.key}
                                    onClick={() => {
                                            router.push(menu.url)
                                    }}
                                    style={{padding: 0}}
                                >
                                        <Link href={menu.url}>
                                                <div className={'flex flex-row items-center lg:mx-5 '}>
                                                        {menu.icon}
                                                        <Typography.Text className={'text-white'}>{menu.name}</Typography.Text>
                                                </div>
                                        </Link>
                                </Menu.Item>
                        })}
        </Menu>
};

export default LayoutSide;

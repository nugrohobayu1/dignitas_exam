import React from "react";
import LayoutMain from "./LayoutMain";

const DefaultLayout = (props) => {
    return <LayoutMain {...props}/>
}

export default DefaultLayout;
import React from "react";
import {Card, Col, Row} from "antd";
import LayoutMain from "../../component/layout/LayoutMain";
import DefaultLayout from "../../component/layout/DefaultLayout";

const Dashboard = () =>{
    return (
        <DefaultLayout>
          <div className="site-card-wrapper mx-4 my-4">
        <Row gutter={16}>
            
            <Col span={8}>
                <Card hoverable title="Total Pemesanan">
                    Data Total Pemesanan : <b>100 Pesanan</b>
                </Card>
            </Col>
            <Col span={8}>
                <Card hoverable title="Total Penjualan" bordered={false}>
                    Data Total Penjualan : <b>500 PCS</b>
                </Card>
            </Col>
            <Col span={8}>
                <Card hoverable title="Total Pelanggan" bordered={false}>
                   Data Total Pelanggan : <b>1.000 Pelanggan</b>
                </Card>
            </Col>
        </Row>
    </div>
    </DefaultLayout>
    )
    
  
}

export default Dashboard;

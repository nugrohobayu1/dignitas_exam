const { useRouter } = require("next/router");

const nomor = ()=>{
    const datas = [
        {
          key: '1',
          nomor: 1,
          nama: 'Dian Bayu Nugroho',
          tanggal: '01 Juli 2022',
          jumlah: 'Rp. 100.000',
        },
        {
          key: '2',
          nomor: 2,
          nama: 'Bayu Nugroho',
          tanggal: '11 Juli 2022',
          jumlah: 'Rp. 200.000',
        },
        {
          key: '3',
          nomor: 3,
          nama: 'Nugroho',
          tanggal: '15 Juli 2022',
          jumlah: 'Rp. 100.000',
        },
      ];

    const router = useRouter()
    const {nomor} = router.query

    const dataSelected = datas.find(data => data.nomor == nomor)


    return(
        <div>
            <p><b>Nomor Pemesanan :</b> {dataSelected?.nomor} </p>
            <p><b> Nama Pelanggan :</b> {dataSelected?.nama} </p>
            <p><b> Tanggal Pembelian :</b> {dataSelected?.tanggal} </p>
            <p><b> Jumlah Pembelian :</b> {dataSelected?.jumlah} </p>
            
        </div>
    ) 
       
}


export default nomor;
import DefaultLayout from "../../component/layout/DefaultLayout";
import { Button, Table  } from 'antd';
import React from 'react';
import Link from 'next/link'
const columns = [
  {
    title: 'Nomor Pemesanan',
    dataIndex: 'nomor',
    key: 'nomor',
    
  },
  {
    title: 'Nama Customer',
    dataIndex: 'nama',
    key: 'nama',
    render: (text) => <a>{text}</a>,
  },
  {
    title: 'Tanggal Pembelian',
    dataIndex: 'tanggal',
    key: 'tanggal',
  },
  {
    title: 'Jumlah Pembayaran',
    dataIndex: 'jumlah',
    key: 'jumlah',
  },
  
  {
    title: 'Action',
    key: 'action',
    render: (_, record) => (
        <Button>
            <Link href={`/pemesanan/${record.nomor}`}>Detail</Link>
        </Button>
    ),
  },
];
const data = [
  {
    key: '1',
    nomor: 1,
    nama: 'Dian Bayu Nugroho',
    tanggal: '01 Juli 2022',
    jumlah: 'Rp. 100.000',
  },
  {
    key: '2',
    nomor: 2,
    nama: 'Bayu Nugroho',
    tanggal: '11 Juli 2022',
    jumlah: 'Rp. 200.000',
  },
  {
    key: '3',
    nomor: 3,
    nama: 'Nugroho',
    tanggal: '15 Juli 2022',
    jumlah: 'Rp. 100.000',
  },
];
const Pemesanan = () =>{
    return(
        <DefaultLayout>
            <div className="mx-4 my-4"> 
            <Table columns={columns} dataSource={data} />
            </div>
            
        </DefaultLayout>
    )
}
export default Pemesanan;
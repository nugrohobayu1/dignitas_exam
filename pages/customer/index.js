import DefaultLayout from "../../component/layout/DefaultLayout";
import { Button, Modal, Table, Form, Input } from 'antd';
import React from 'react';
import { useState } from 'react';
import ModalForm from "./modalForm";

const columns = [
    {
        title: 'Nama Customer',
        dataIndex: 'nama',
        key: 'nama',
        render: (text) => <a>{text}</a>,
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        
      },
      {
        title: 'NIK',
        dataIndex: 'nik',
        key: 'nik',
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
      },
      
];
const data = [
  {
    key: '1',
    nama: 'Dian Bayu Nugroho',
    email: "d.bayu@gmail.com",
    nik: 12345345563,
    status:'Belum Menikah',
  },
  {
    key: '2',
    nama: 'Bayu Nugroho',
    email: "d.bayu@gmail.com",
    nik: 98345345563,
    status:'Belum Menikah',
  },
  {
    key: '3',
    nama: 'Nugroho',
    email: "d.bayu@gmail.com",
    nik: 4565345563,
    status:'Belum Menikah',
  },

];

const Customer = () =>{

 const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

    return(
        <DefaultLayout>
              <div className="mx-4 my-4">            
              <Button className="bg-sky-500 rounded-md mb-1" onClick={showModal}>
                    Tambah Data
              </Button>

              <Modal title="Tambah Data Customer" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                <div className="mx-8">
                  <ModalForm/>

                </div>
             
              </Modal>
              
                <Table columns={columns} dataSource={data} />
              </div>
        </DefaultLayout>
    )
}
export default Customer;

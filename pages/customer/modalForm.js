import { Button, Modal, Table, Form, Input } from 'antd';
import React from 'react';
import { useState } from 'react';

export default function ModalForm(){
    return(
        <div>
         <Form
                name="basic"
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                initialValues={{ remember: true }}
                autoComplete="off"
                >
                <Form.Item
                    label="Nama Customer"
                    name="nama"
                    rules={[{ required: true, message: 'Please input your username!' }]}
                >
                    <Input className="w-full"/>
                </Form.Item>

                <Form.Item
                    label="E-mail"
                    name="email"
                    rules={[{ required: true, message: 'Please input your E-mail!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="NIK"
                    name="nik"
                    rules={[{ required: true, message: 'Please input your NIK!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    label="Konfirmasi Password"
                    name="konfirmasi_password"
                    rules={[{ required: true, message: 'Please konfirm your password!' }]}
                >
                    <Input.Password />
                </Form.Item>
                </Form>
        </div>
    )
}